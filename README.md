# Form elements #

> scss/_form.scss


- **form__alert** - Общая ошибка формы  
- **form__row** - Строка формы  
- **form__control** - Обертка, внутри которой выводится ошибка для конкретного элмента/элементов формы.
    - **form__control--error** - модификатор, при наличии которого **блок с ошибкой показан**  
- **form__elem** - Обертка элемента формы. Может быть тегом <label>. Если, к примеру, внутри него чекбокс / радио
- **form__label** - к тегу label не имеет никакого отношения. Это просто блок с текстом   
- **form__input** - обертка для input [type= "text / email / password / tel / number / url / date / ..."], обычно является тегом <label>. 
(Может иметь модификатор: **form__input--text** / **form__input--email** и т.д.)
- **form__textarea** - обертка для <textarea>
- **form__select** - обертка для <select>
- **form__error** - блок в котором выводится ошибка для **form__control**.
(**По умолчанию скрыт**, показывается только внутри блока **.form__control.form__control--error**)
